public class TestAccount {

    public static void main(String[] args) {

//        Account myAccount = new Account();
//        myAccount.setName("Ronan Whoriskey");
//        System.out.println("Account Holder: " + myAccount.getName());
//        System.out.println("Initial Balance: " + myAccount.getBalance());
//        myAccount.setBalance(10000);
//        System.out.println("Balance after First Deposit: " + myAccount.getBalance());
//        myAccount.addInterest();
//        System.out.println("Balance with 10% interest added: " + myAccount.getBalance());

        Account[] arrayOfAccounts;          // array declaration
        arrayOfAccounts = new Account[5];   // initialising the array to hold 5 accounts

        double[] amounts = {10, 100, 1000, 10000, 100000};
        String[] names = {"Shan", "Kiera", "Topher", "Colette", "Ronan"};

        // initialise the array of accounts contents to contain account objects
        for (int i = 0; i < arrayOfAccounts.length; i++)
        {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            System.out.println("The Account Holder is: " +
                    arrayOfAccounts[i].getName());
            System.out.println("The Initial Balance is: £" +
                    arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("The New Balance PLUS Interest is: £" +
                    arrayOfAccounts[i].getBalance() + "\n");
        }
    }
}
